/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Set the date we're counting down to
var countDownDate = new Date(new Date().getTime()).getTime();

var timer = getRandomInt(10000, 300000);

if ($.cookie('countDownDate') > 0) {
    countDownDate = $.cookie('countDownDate');
    timer = $.cookie('timer');
} else {
    countDownDate += timer;
}

var now = new Date().getTime();
var distance2 = countDownDate - now;
var timeout = distance2 + now;
document.getElementById("timeout").innerHTML = new Date(timeout).getHours() + ":" + new Date(timeout).getMinutes();
$.cookie('countDownDate', countDownDate, {
    expires: 7
});
$.cookie('timer', timer, {
    expires: 7
});

// Update the count down every 1 second
var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();
    // Find the distance between now and the count down date

    distance = countDownDate - now;

    var perc = Math.abs((distance / timer * 100) - 100);
    var perc2 = Math.abs((distance / timer * 100));
    $('.progress-bar').width(perc + '%');
    $(".lottiePlayer").animate({
        left: perc + "%",
    });
    // $(".lottiePlayer").css("left",perc + "%");
    if (perc <= 100)
        $('.progress-bar').text(Math.round(perc) + '%');
    else {
        $('.progress-bar').text('100%');
        $('.progress-bar').width('100%');
    }


    // Time calculations for days, hours, minutes and seconds
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    $('.line_no').text(Math.round(perc2 + timer / 10000 * 2));

    // Display the result in the element with id="demo"
    document.getElementById("demo").innerHTML = +hours + "h " +
        minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        $('.timer').hide();
        $('.body, body').css('background-color', '#fff');
        $('body').css('background-image', 'unset');
        $('.main').fadeIn();
        document.getElementById("demo").innerHTML = "Go to product Page";
        $("#enter").html("<a href='"+gotoUrl+"'>Enter Site Now</a>");
        $("#enter").addClass("btn-success");
        $.cookie('queue_status', 'OK', {
            expires: 7
        });
    }
}, 1000);

// Language Selector
$(document).on("click", ".lang-selector", function(){
    let lang = $(this).attr("data-attr");
    $.cookie('lang', lang, {
        expires: 3600
    });
    location.reload();
});