<?php require_once("./config.php"); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>1-Click KOL Shop</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="./assets/favicon.ico">
        <!-- Adding Stylesheets -->
        <link rel="stylesheet" href="./assets/vendor/bootstrap/css/bootstrap.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="./css/style.css">
    </head>
    <body>
        <header>
            <div class="container">
                <div class="header_items justify-content-between">
                    <div class="logo"><a href="#"><img src="./assets/logo.png" alt="logo"></a></div>
                    <div class="language_d">
                        <div class="dropdown">
                            <button type="button" class="" data-toggle="dropdown">
                                <span class="langSelector">
                                    <?php if ($_COOKIE['lang'] == 'English') { ?>
                                        English
                                    <?php } else if ($_COOKIE['lang'] == '中文(繁體)') { ?>
                                        中文(繁體)
                                    <?php } else { ?>
                                        English
                                    <?php } ?>
                                </span>
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item lang-selector" data-attr="English">English</a>
                                <a class="dropdown-item lang-selector" data-attr="中文(繁體)">中文(繁體)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="que_screen">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-6 que-bg">
                        <iframe src="https://www.youtube.com/embed/SnMoDDbEccE" style="height:54%;width:100%;" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-lg-6">
                        <div class="que_content">
                            <h1>Your are now in line</h1>
                            <h2>Your are now in line. Please kindly wait for your turn and don't leave this page.</h2>
                            <a href="#.">What is this?</a>
                            <div class="meter">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                <lottie-player class="lottiePlayer" src="./assets/walk.json" style="height:100%;" autoplay loop></lottie-player>
                            </div>
                            <p>Estimated time entering site: <span id="timeout"></span></p>
                            <p>Estimated waiting time: <span id="demo"></span></p>
                            <button type="button" id="enter" class="btn btn-default">Enter Site Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="footer-links">
                    <ul>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">T&C</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                </div>
                <p>Created by &copy; Kol.SHOP</p>
            </div>
        </footer>
        <script src="https://unpkg.com/@lottiefiles/lottie-player@0.4.0/dist/lottie-player.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="./assets/vendor/jquery/jquery.min.js"></script>
        <script src="./assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="./assets/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="./assets/js/examples/examples.gallery.js"></script>
        <script src="./assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="./assets/vendor/jcookie/src/jquery.cookie.js"></script>

        <!-- Sending redirect URL to Script -->
        <script> var gotoUrl = "<?= $gotoUrl ?>"; </script>
        <script src="./js/script.js"></script>
    </body>
</html>